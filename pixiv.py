import requests
import re
import os
import json
import datetime

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36',
    'referer': 'https://www.pixiv.net/ranking.php?mode=daily&content=illust',
}

path = './'
repeat = 1

def getSinglePage(id, pageUrl, count):
    global repeat
    response = requests.get(pageUrl, headers=headers)
    # 提取图片名称
    pageName = re.search('"illustTitle":"(.+?)"', response.text)
    pageName = pageName.group(1)
    if re.search('[\\\ \/ \* \? \" \: \< \> \|]', pageName) != None:
        pageName = re.sub('[\\\ \/ \* \? \" \: \< \> \|]', str(repeat), pageName)
        repeat += 1
    pageName += ' -'
    
    url = 'https://www.pixiv.net/ajax/illust/{}/pages?lang=zh'.format(id)
    response = requests.get(url,headers=headers).text
    res_json = json.loads(response)

    body = res_json['body']
    for datas in body:
        download_url = datas['urls']['original']
        name = str(count) + '_' + download_url.split('/')[-1].split('_')[-1]
        pageName += (' ' + name)
        content = requests.get(download_url,headers=headers).content
        with open(name,mode='wb')as f:
            f.write(content)
    return pageName

def getAllPicUrl():
    global repeat
    count = 1
    weekUrl = 'https://www.pixiv.net/ranking.php?mode=daily&content=illust&p=1&format=json'
    response = requests.get(weekUrl, headers=headers)

    illust_id = re.findall('"illust_id":(\d+?),', response.text)
    ids = [i for i in illust_id]

    date_p = datetime.datetime.now().date()  # 获取当前日期,date类型
    date_s = str(date_p)

    with open('pixiv.html','w') as f:
        for id in ids:
            print('%d' % count, end='   ')
            pageUrl = 'https://www.pixiv.net/artworks/' + str(id)
            name = getSinglePage(id, pageUrl, count)
            print('下载成功', end='\n')
            f.write(str(name) + ' - ' + pageUrl + '\n')
            count += 1
            if count == 4:
                break
        f.write(date_s)
    return None
    
files= os.listdir(path) #得到文件夹下的所有文件名称
s = []
for file in files: #遍历文件夹
     if not os.path.isdir(file): #判断是否是文件夹
          if file != 'pixiv.py' and file != 'pixiv.html':
              os.remove(file)

getAllPicUrl()
